ARGS :=

install:
	ansible-playbook site.yml ${ARGS} --ask-become-pass

reqs:
	ansible-galaxy install -r requirements.yml

force-reqs:
	ansible-galaxy install -r requirements.yml  --force
